package dz1

class Dz1 {

    val parser = JSON()
    parser.readerJson("C:\\Users\\user\\Desktop\\test.json")

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {

            test()
        }

        fun test(){

            val testSet: Set<String> = setOf("z", "x", "c")
            val testmap1: Map<String, Any> = mapOf("q" to "w", "s" to testSet)
            val testMap: Map<String, Any> = mapOf("a" to "b", "12" to "11", "qwe" to testSet, "opa" to testmap1)


            val jsonTest = Json()
            val jstr = jsonTest.serialize(testMap)
            println(jstr)
            val path = "customJson.txt"
            jsonTest.writeFile(path, jstr)


        }
    }
}