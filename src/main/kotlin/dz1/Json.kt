package dz1

import java.io.File
import java.io.FileWriter

class Json {

    //создание map
    public fun createMap(): MutableMap<String,Any?> {
        var map = mutableMapOf<String, Any?>()
        return map
    }

    //создание set
    public  fun createSet(): MutableSet<Any?> {
        var set = mutableSetOf<Any?>()
        return  set
    }

    // получение строки внутри ""
    public fun  createString(reader: InputStreamReader): String {
        var str = ""
        while(true){
            var c = reader.read()
            if (c.toChar() == '"') break
            str += c.toChar().toString()
        }
        return str
    }

    // получение value из цифр
    public fun  createNumeralString(reader: InputStreamReader, firstNumber: Char): String {
        var str = ""
        str += firstNumber.toString()
        while(true){
            var c = reader.read()
            if (c.toChar() == ',') break
            str += c.toChar().toString()
        }
        return str
    }

    // определение value: map, set или string
    public  fun readValue(reader: InputStreamReader): Any? {
        var value:Any? = null
        while (true) {
            var c = reader.read()
            if (c.toChar() == '{') {
                value = createMap()
                break
            }
            else if (c.toChar() == '[') {
                value = createSet()
                break
            }
            else if (c.toChar() == ']'){
                value = ']' // конец set
                break
            }
            else if (c.toChar() == '"'){
                value = createString(reader)
                break
            }
            else if (c.toChar() in CharRange('0', '9')){ // value значения которые пишутся без ""
                val firstNumber = c.toChar()
                value = createNumeralString(reader, firstNumber)
                break
            }
        }
        return value
    }

    //основной цикл
    public  fun readerJson(path: String) {

        val reader = File(path).reader()  // uses UTF-8 by default

        var root:MutableMap<String, Any?>? = null

        var key:String = ""
        var value:Any? = null
        var currentCollection: Any? = null

        var arrayCollection = mutableListOf<Any?>() // array для коллекций, что вернуться

        reader.use {
            while (true) {
                val c = reader.read()
                if (EOF == c) break

                else if (c.toChar() == '"'){
                    key = createString(reader)
                }

                else if (c.toChar() == ':'){ // читаем value
                    value = readValue(reader)
                    if (value is MutableMap<*,*>) {
                        (currentCollection as MutableMap<String, Any?>)?.put(key, value)
                        arrayCollection.add(currentCollection)
                        currentCollection = (currentCollection as MutableMap<String, Any?>).getValue(key)
                    }
                    if (value is String) {
                        (currentCollection as MutableMap<String, Any?>)?.put(key, value)
                    }
                    if (value is MutableSet<*>) {

                        var set = mutableSetOf<Any?>()

//                        arrayCollection.add(currentCollection)
//                        currentCollection = value
//                        arrayCollection.add(currentCollection)

                        while (true){
                            var valueset = readValue(reader)
                            if (valueset != ']') {
                                set.add(valueset)
                            }
                            else if (valueset == ']'){
                                (currentCollection as MutableMap<String, Any?>)?.put(key, set)
                                break
                            }
                        }
                    }
                }

                else if (c.toChar() == '}'){
                    if (arrayCollection.size != 0) {
                        currentCollection = arrayCollection.last() // в случае если в map вложен еще один map
                        arrayCollection.remove(arrayCollection.last())
                    }
                }

                else if (c.toChar() == '{') { // создание первоначального map
                    if(root == null){
                        root = createMap()
                        currentCollection = mutableMapOf<String, Any?>()
                        currentCollection = root
                    }
                }

                else if (c.toChar() == '[') { // создание первоначальной коллекции, set
                    if(root == null){
                        root == createSet()
                        currentCollection = arrayListOf<Any?>()
                        currentCollection = root
                    }
                }
            }
        }
    }

////////////////////////////////////////////////////////////////////////////////

    fun serialize(root: Any): String{
//        if (root != null){
            var str = ""
            if (root is Map<*,*>){
                str = "{${buildStringMap(root)}}"
                return str
            }

            else if (root is Set<*>){
                str = "[${buildStringSet(root)}]"
            }

            return str
//        }

        return ""
    }

    fun writeFile(path: String, content: String){
        File(path).writeText(content)
    }

    private fun buildStringMap(c: Map<*,*>): String {

        var str = ""
        val cSize = c.size
        var i = 0
        for ((key,value) in c) {
            if (value is Map<*,*>){

                str += "\"$key\":{${buildStringMap(value)}}"
            }

            else if (value is Set<*>){

                str += "\"$key\":[${buildStringSet(value)}]"
            }

            else{
                str += "\"$key\":\"$value\""
            }

            if ((i++) < cSize-1) str += ","

        }

        return str
    }

    private fun buildStringSet(c: Set<*>): String {

        var str = ""
        val cSize = c.size
        var i = 0

        for (value in c){
            if (value is Map<*,*>){
                str += "{${buildStringMap(value)}}"
            }
            else if (value is Set<*>){
                str += "{${buildStringSet(value)}}"
            }
            else{
                str += "\"$value\""
            }

            if ((i++) < cSize-1) str += ","
        }


        return str
    }



}